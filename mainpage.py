## @file mainpage.py
#
#
#
#   @mainpage
#
#   @author Ermen Elenes-Cazares
#
#   @section sec_intro Introduction
#   Text contents of introduction section
#
#   @page page_testing Testing
#
#   @section sec_testing Firmware Testing
#   Text contents.... We can refer to other sections like @ref sec_intro
#
#   @page page_fibonacci Fibonacci
#
#   @section Fibonacci_Sequence Source Code Access
#   You can find the source code for the fibonacci program here:
#   https://bitbucket.org/eelenesc/me305/src/master/Lab1/Lab1Fib_gen.py
#
#   @section page_fib_doc Documentation
#   The documentation for the fibonacci program is located in
#   the files folder labled Lab1Fib_gen.Fibonacci.
#
#   @page page_elevator ElevatorFSM
#
#   @section ElevatorFSM Source Code Access 
#   You can find the source code for the elevator finite state machine program here:
#   
#
#   @section page_elevator_doc Documentation
#   The documentation for the Elevator Finite State Machine program is located in
#   
#
#   @page page_nucleomain NucleoLED
#
#   @section Nucleo blinking LED Source Code Access 
#   You can find the source code for the Nucleo blinking LED finite state machine program here:
#   
#
#   @section page_nucleomain_doc Documentation
#   The documentation for the Nucleo blinking LED finite state machine program is located in
#   
#   @page page_Nucleo Nucleo Encoder
#
#   @section Nucleo_Encoder Source Code Access 
#   You can find the source code for the Nucleo Encoder: 
#   https://bitbucket.org/eelenesc/me305/src/master/Lab3/
#
#   @section page_NEncoder_doc Documentation
#   The documentation for the encoder for the nucleo.
#
#   
#   @page page_ExtInterface UI Data Generator
#
#   @section Data Generator Source Code Access 
#   You can find the source code and the task and state diagrams for the UI Data Generator: 
#   https://bitbucket.org/eelenesc/me305/src/master/Lab4/
#
#   @section page_Data_Generator_doc Documentation
#   The documentation for the Data Generator.
#
#   
#   @page page_LEDBLE Bluetooth LED Control
#
#   @section Bluetooth LED Control Source Code Access 
#   You can find the source code and the task and state diagrams for the UI Data Generator: 
#   https://bitbucket.org/eelenesc/me305/src/master/Lab5/
#
#   @section page_LEDBLE_doc Documentation
#   The documentation for the Bluetooth Led Control.
#
#   