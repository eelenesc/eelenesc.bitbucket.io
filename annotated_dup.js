var annotated_dup =
[
    [ "CLController", null, [
      [ "Controller", "classCLController_1_1Controller.html", "classCLController_1_1Controller" ]
    ] ],
    [ "ClosedLoop", null, [
      [ "ClosedLoop", "classClosedLoop_1_1ClosedLoop.html", "classClosedLoop_1_1ClosedLoop" ]
    ] ],
    [ "DGenFlashed", null, [
      [ "DGenTask", "classDGenFlashed_1_1DGenTask.html", "classDGenFlashed_1_1DGenTask" ]
    ] ],
    [ "DGenFlashed6", null, [
      [ "DGenTask", "classDGenFlashed6_1_1DGenTask.html", "classDGenFlashed6_1_1DGenTask" ]
    ] ],
    [ "DGenFlashed7", null, [
      [ "DGenTask7", "classDGenFlashed7_1_1DGenTask7.html", "classDGenFlashed7_1_1DGenTask7" ]
    ] ],
    [ "ElevatorFSM", null, [
      [ "Button", "classElevatorFSM_1_1Button.html", "classElevatorFSM_1_1Button" ],
      [ "MotorDriver", "classElevatorFSM_1_1MotorDriver.html", "classElevatorFSM_1_1MotorDriver" ],
      [ "TaskElevator", "classElevatorFSM_1_1TaskElevator.html", "classElevatorFSM_1_1TaskElevator" ]
    ] ],
    [ "FrontEndTask", null, [
      [ "UITask", "classFrontEndTask_1_1UITask.html", "classFrontEndTask_1_1UITask" ]
    ] ],
    [ "FrontEndTask6", null, [
      [ "UITask", "classFrontEndTask6_1_1UITask.html", "classFrontEndTask6_1_1UITask" ]
    ] ],
    [ "FrontEndTask7", null, [
      [ "UITask", "classFrontEndTask7_1_1UITask.html", "classFrontEndTask7_1_1UITask" ]
    ] ],
    [ "MotorDriver", null, [
      [ "MotorDriver", "classMotorDriver_1_1MotorDriver.html", "classMotorDriver_1_1MotorDriver" ]
    ] ],
    [ "NEncoder", null, [
      [ "EncoderDriver", "classNEncoder_1_1EncoderDriver.html", "classNEncoder_1_1EncoderDriver" ],
      [ "EncoderTask", "classNEncoder_1_1EncoderTask.html", "classNEncoder_1_1EncoderTask" ]
    ] ],
    [ "NEncoderLab6", null, [
      [ "EncoderDriver", "classNEncoderLab6_1_1EncoderDriver.html", "classNEncoderLab6_1_1EncoderDriver" ],
      [ "EncoderTask", "classNEncoderLab6_1_1EncoderTask.html", "classNEncoderLab6_1_1EncoderTask" ]
    ] ],
    [ "TaskUserInterface", null, [
      [ "TaskUser", "classTaskUserInterface_1_1TaskUser.html", "classTaskUserInterface_1_1TaskUser" ]
    ] ],
    [ "ClosedLoop", "classClosedLoop.html", null ],
    [ "DGenTask7", "classDGenTask7.html", null ],
    [ "MotorDriver", "classMotorDriver.html", null ]
];