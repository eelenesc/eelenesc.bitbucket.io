/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Mechatronics Portfolio for Ermen Elenes-Cazares", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Fibonacci", "page_fibonacci.html", [
      [ "Source Code Access", "page_fibonacci.html#Fibonacci_Sequence", null ],
      [ "Documentation", "page_fibonacci.html#page_fib_doc", null ],
      [ "Instructions", "page_fibonacci.html#page_fib_Instruc", null ]
    ] ],
    [ "ElevatorFSM", "page_elevator.html", [
      [ "Source Code Access", "page_elevator.html#ElevatorFSM", null ],
      [ "Documentation", "page_elevator.html#page_elevator_doc", null ]
    ] ],
    [ "NucleoLED", "page_nucleomain.html", [
      [ "blinking LED Source Code Access", "page_nucleomain.html#Nucleo", null ],
      [ "Documentation", "page_nucleomain.html#page_nucleomain_doc", null ]
    ] ],
    [ "Nucleo Encoder", "page_Nucleo.html", [
      [ "Source Code Access", "page_Nucleo.html#Nucleo_Encoder", null ],
      [ "Documentation", "page_Nucleo.html#page_NEncoder_doc", null ],
      [ "Instructions", "page_Nucleo.html#page_NEncoder__Instruc", null ]
    ] ],
    [ "UI Data Generator", "page_ExtInterface.html", [
      [ "Generator Source Code Access", "page_ExtInterface.html#Data", null ],
      [ "Documentation", "page_ExtInterface.html#page_Data_Generator_doc", null ],
      [ "Instructions", "page_ExtInterface.html#page_Data_Generator_Instruc", null ]
    ] ],
    [ "Bluetooth LED Control", "page_LEDBLE.html", [
      [ "LED Control Source Code Access", "page_LEDBLE.html#Bluetooth", null ],
      [ "Documentation", "page_LEDBLE.html#page_LEDBLE_doc", null ],
      [ "Instructions", "page_LEDBLE.html#page_LEDBLE_Instruc", null ]
    ] ],
    [ "Closed Loop Controller", "page_Closed_Loop_Controller.html", [
      [ "Loop Controller Source Code Access", "page_Closed_Loop_Controller.html#Closed", null ],
      [ "Documentation", "page_Closed_Loop_Controller.html#page_Closed_Loop_Controller_doc", null ],
      [ "Instructions", "page_Closed_Loop_Controller.html#page_Closed_Loop_Controlle_Instruc", null ]
    ] ],
    [ "Data Tracer for Motor Program", "page_Lab_Tracer_Program.html", [
      [ "Source Code Access", "page_Lab_Tracer_Program.html#page_Lab_Tracer_Program_sca", null ],
      [ "Documentation", "page_Lab_Tracer_Program.html#page_Lab_Tracer_Program_doc", null ],
      [ "Instructions", "page_Lab_Tracer_Program.html#page_Lab_Tracer_Program_Instruc", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"CLController_8py.html",
"classFrontEndTask7_1_1UITask.html#ae04bbd9c06ced1768b52e12cc76b37d4"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';