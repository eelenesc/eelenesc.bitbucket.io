# -*- coding: utf-8 -*-
"""
Created on Mon Sep 28 18:32:42 2020

@author: 1
"""
'''
@file       Lab1Fib_gen
@brief      A file focused on the Fibonacci sequence.
@details    This is a program that returns Fibonacci values corresponding to an 
            index entered in by a user.
@author     Ermen Elenes-Cazares
@copyright  
'''
   
def fib_gen():
    userval = 'y'
    while userval in ('y'):
        try:
            n = int(input("Please enter an index value: "))
            a, b, c = 0, 1, 0
            #The first few statements are for specific values 
            #Specifically negative numbers, 0, 1
            if n < 0:
                    print ("The index value must be at least 0")
            elif n == 0:
                    print ("The Fibonacci number for index 0 is 0")
            elif n == 1:
                    print ("The Fibonacci number for index ", n,"is:")
                    print (b)
            else:
                    print("You inputed index value", n, ". Calculating...")    
                    while c < n:
                        #print (a)
                        #The above line is commented out in order to simplify 
                        #the output. If needed to show the values before, it can
                        #be uncommented out of the program. 
                        fib = a + b  #Here we have the recursive element of the eq
                        a = b
                        b = fib
                        c +=1 #c acts as the counter
                    print ("The Fibonacci value is: ", a)            
            while True: 
                #this loop allows us to run the function again if wanted by
                #gathering input from user.
                        userval = input("Do you wish to continue? (y/n): ")
                        if userval in ('y', 'n'):
                            break
                        print ("Not a valid option.")
                        if userval == 'y':
                            continue 
                        else: 
                            print ("Program over.")
                            break
        except:
            print("Please enter an integer and try again.")
             