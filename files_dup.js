var files_dup =
[
    [ "CLController.py", "CLController_8py.html", [
      [ "Controller", "classCLController_1_1Controller.html", "classCLController_1_1Controller" ]
    ] ],
    [ "ClosedLoop.py", "ClosedLoop_8py.html", [
      [ "ClosedLoop", "classClosedLoop_1_1ClosedLoop.html", "classClosedLoop_1_1ClosedLoop" ]
    ] ],
    [ "DGenFlashed.py", "DGenFlashed_8py.html", [
      [ "DGenTask", "classDGenFlashed_1_1DGenTask.html", "classDGenFlashed_1_1DGenTask" ]
    ] ],
    [ "DGenFlashed7.py", "DGenFlashed7_8py.html", [
      [ "DGenTask7", "classDGenFlashed7_1_1DGenTask7.html", "classDGenFlashed7_1_1DGenTask7" ]
    ] ],
    [ "EncoderMain.py", "EncoderMain_8py.html", "EncoderMain_8py" ],
    [ "FrontEndTask.py", "FrontEndTask_8py.html", [
      [ "UITask", "classFrontEndTask_1_1UITask.html", "classFrontEndTask_1_1UITask" ]
    ] ],
    [ "FrontEndTask6.py", "FrontEndTask6_8py.html", [
      [ "UITask", "classFrontEndTask6_1_1UITask.html", "classFrontEndTask6_1_1UITask" ]
    ] ],
    [ "FrontEndTask7.py", "FrontEndTask7_8py.html", [
      [ "UITask", "classFrontEndTask7_1_1UITask.html", "classFrontEndTask7_1_1UITask" ]
    ] ],
    [ "GlobalVars.py", "GlobalVars_8py.html", "GlobalVars_8py" ],
    [ "main5.py", "main5_8py.html", "main5_8py" ],
    [ "MainFlashed.py", "MainFlashed_8py.html", "MainFlashed_8py" ],
    [ "MotorDriver.py", "MotorDriver_8py.html", [
      [ "MotorDriver", "classMotorDriver_1_1MotorDriver.html", "classMotorDriver_1_1MotorDriver" ]
    ] ],
    [ "NEncoder.py", "NEncoder_8py.html", [
      [ "EncoderDriver", "classNEncoder_1_1EncoderDriver.html", "classNEncoder_1_1EncoderDriver" ],
      [ "EncoderTask", "classNEncoder_1_1EncoderTask.html", "classNEncoder_1_1EncoderTask" ]
    ] ],
    [ "NEncoderLab6.py", "NEncoderLab6_8py.html", [
      [ "EncoderDriver", "classNEncoderLab6_1_1EncoderDriver.html", "classNEncoderLab6_1_1EncoderDriver" ],
      [ "EncoderTask", "classNEncoderLab6_1_1EncoderTask.html", "classNEncoderLab6_1_1EncoderTask" ]
    ] ],
    [ "Shares.py", "Shares_8py.html", "Shares_8py" ],
    [ "shares6.py", "shares6_8py.html", "shares6_8py" ],
    [ "shares7.py", "shares7_8py.html", "shares7_8py" ],
    [ "TaskUserInterface.py", "TaskUserInterface_8py.html", [
      [ "TaskUser", "classTaskUserInterface_1_1TaskUser.html", "classTaskUserInterface_1_1TaskUser" ]
    ] ],
    [ "UILaptopTestMain.py", "UILaptopTestMain_8py.html", "UILaptopTestMain_8py" ]
];